from unittest import main, TestCase
from models import db, Book
import os, sys

class UnitTests(TestCase):
    def test_source_insert_1(self):
        s = Book(id='20', title= 'C++')
        db.session.add(s)
        db.session.commit()

        r = db.session.query(Book).filter_by(id = '20').one()
        self.assertEqual(str(r.id), '20')

        db.session.query(Book).filter_by(id = '20').delete()
        db.session. commit()

if __name__ == '__main__':
    main()
